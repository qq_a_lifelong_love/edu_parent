package com.sjh.eduorder.controller;


import com.sjh.commonutils.Result;
import com.sjh.eduorder.service.PayLogService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 支付日志表 前端控制器
 * </p>
 *
 * @author 宋军辉
 * @since 2023-03-27
 */
@RestController
@CrossOrigin//解决跨域
@RequestMapping("/eduorder/paylog")
public class PayLogController {

    @Autowired
    private PayLogService payLogService;

    //    生成微信支付二维码接口
//    参数是订单号
    @ApiOperation(value = "生成微信支付二维码接口")
    @GetMapping("/createNative/{orderNo}")
    public Result createNative(@PathVariable String orderNo) {

//        返回信息，包含二维码地址，还有其他需要的信息
        Map map = payLogService.createNative(orderNo);
        return Result.ok().data(map);
    }


    //2，查询订单支付状态
    //参数：订单号，根据订单号查询支付状态
    @ApiOperation(value = "查询订单支付状态")
    @GetMapping("queryPayStatus/{orderNo}")
    public Result queryPayStatus(@PathVariable String orderNo){
        Map<String,String> map = payLogService.queryPayStatus(orderNo);
        if(map == null){
            return Result.error().message("支付出错了");
        }
        if(map.get("trade_state").equals("SUCCESS")){
            //添加记录到支付表，更新订单表订单状态
            payLogService.updateOrderStatus(map);
            return Result.ok().message("支付成功");
        }
        return Result.ok().code(25000).message("支付中");
    }
}

