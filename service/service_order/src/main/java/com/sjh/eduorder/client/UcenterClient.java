package com.sjh.eduorder.client;

import com.sjh.commonutils.ordervo.UcenterMemberOrder;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @Author: sjh
 * @Date: 2023-03-28
 * @Version: 1.0
 */
@Component
@FeignClient("service-ucenter")//调用哪个模块写哪个模块
public interface UcenterClient {

    //    根据用户id获取用户信息
    @ApiOperation(value = "根据用户id获取用户信息")
    @PostMapping("/educenter/member/getUserInfoOrder/{id}")
    public UcenterMemberOrder getUserInfoOrder(@PathVariable("id") String id);
}
