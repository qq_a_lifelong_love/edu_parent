package com.sjh.eduorder;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author: sjh
 * @Date: 2023-03-27
 * @Version: 1.0
 */
@SpringBootApplication
@EnableDiscoveryClient//nacos注册
@EnableFeignClients//服务调用
@ComponentScan(basePackages = {"com.sjh"})
@MapperScan("com.sjh.eduorder.mapper")
public class OrdersApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrdersApplication.class,args);
    }
}
