package com.sjh.eduorder.mapper;

import com.sjh.eduorder.entity.PayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付日志表 Mapper 接口
 * </p>
 *
 * @author 宋军辉
 * @since 2023-03-27
 */
public interface PayLogMapper extends BaseMapper<PayLog> {

}
