package com.sjh.eduorder.client;

import com.sjh.commonutils.ordervo.CourseWebVoOrder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @Author: sjh
 * @Date: 2023-03-28
 * @Version: 1.0
 */
@Component
@FeignClient("service-edu")//调用哪个模块写哪个模块
public interface EduClient {

    //3，根据课程id查询课程的信息
    @PostMapping("/eduservice/coursefront/getCourseInfoOrder/{id}")
    public CourseWebVoOrder getCourseInfoOrder(@PathVariable("id") String id);
}
