package com.sjh.eduorder.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sjh.commonutils.JwtUtils;
import com.sjh.commonutils.Result;
import com.sjh.eduorder.entity.Order;
import com.sjh.eduorder.service.OrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单 前端控制器
 * </p>
 *
 * @author 宋军辉
 * @since 2023-03-27
 */
@RestController
@CrossOrigin//解决跨域
@RequestMapping("/eduorder/order")
public class OrderController {

    @Autowired
    private OrderService orderService;


    //1，生成订单的方法
    @ApiOperation(value = "//生成订单的方法")
    @PostMapping("/createOrder/{courseId}")
    public Result saveOrder(@PathVariable String courseId, HttpServletRequest request){

//        创建订单，返回订单号
        String orderNo = orderService.createOrders(courseId, JwtUtils.getMemberIdByJwtToken(request));
        return Result.ok().data("orderId",orderNo);

    }

//    2根据订单id查询订单信息
    @ApiOperation(value = "2根据订单id查询订单信息")
    @GetMapping("/getOrderInfo/{orderId}")
    public Result getOrderInfo(@PathVariable String orderId){
        QueryWrapper<Order> wrapper=new QueryWrapper<>();
        wrapper.eq("order_no",orderId);
        Order order = orderService.getOne(wrapper);
        return Result.ok().data("item",order);
    }



    //3，根据课程id和用户id查询订单表中订单状态
    @GetMapping("/isBuyCourse/{courseId}/{memberId}")
    public boolean isBuyCourse(@PathVariable String courseId,@PathVariable String memberId){
        QueryWrapper<Order> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",courseId);
        wrapper.eq("member_id",memberId);
        wrapper.eq("status",1);
        int count = orderService.count(wrapper);
        if(count == 0){
            return false;
        }
        return true;
    }


}

