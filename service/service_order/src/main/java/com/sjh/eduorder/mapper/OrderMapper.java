package com.sjh.eduorder.mapper;

import com.sjh.eduorder.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author 宋军辉
 * @since 2023-03-27
 */
public interface OrderMapper extends BaseMapper<Order> {

}
