package com.sjh.eduorder.service;

import com.sjh.eduorder.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 订单 服务类
 * </p>
 *
 * @author 宋军辉
 * @since 2023-03-27
 */
public interface OrderService extends IService<Order> {

//    生成订单的方法
    String createOrders(String courseId, String memberIdByJwtToken);
}
