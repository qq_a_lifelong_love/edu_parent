package com.sjh.educms.service.impl;

import com.sjh.educms.entity.CrmBanner;
import com.sjh.educms.mapper.CrmBannerMapper;
import com.sjh.educms.service.CrmBannerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务实现类
 * </p>
 *
 * @author 宋军辉
 * @since 2023-03-24
 */
@Service
public class CrmBannerServiceImpl extends ServiceImpl<CrmBannerMapper, CrmBanner> implements CrmBannerService {

    //    查询所有的banner
    @Cacheable(key = "'selectIndexList'",value = "banner")//开启缓存的查询方法
    @Override
    public List<CrmBanner> selectAllBanner() {

        List<CrmBanner> list = baseMapper.selectList(null);
        return list;
    }
}
