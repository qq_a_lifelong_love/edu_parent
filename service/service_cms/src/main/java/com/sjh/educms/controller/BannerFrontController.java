package com.sjh.educms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sjh.commonutils.Result;
import com.sjh.educms.entity.CrmBanner;
import com.sjh.educms.service.CrmBannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author 宋军辉
 * @since 2023-03-24
 */
@Api(description = "网站首页Banner列表")
@RestController
@RequestMapping("/educms/bannerfront")
@CrossOrigin//解决跨域
public class BannerFrontController {

    @Autowired
    private CrmBannerService bannerService;

//主要是查询轮播图

    //    查询所有的banner
    @ApiOperation(value = "查询所有的banner")
    @GetMapping("/getAllBanner")
    public Result getAllBanner() {

//        根据id进行降序排列，显示排列之后前两条记录
        QueryWrapper<CrmBanner> wrapper=new QueryWrapper<>();
        wrapper.orderByDesc("id");
//        last()方法，适用于拼接sql语句的，下方意思是说只要前两个
        wrapper.last("limit 2");

        List<CrmBanner> list = bannerService.selectAllBanner();
        return Result.ok().data("list", list);
    }

}

