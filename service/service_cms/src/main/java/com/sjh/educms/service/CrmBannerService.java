package com.sjh.educms.service;

import com.sjh.educms.entity.CrmBanner;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务类
 * </p>
 *
 * @author 宋军辉
 * @since 2023-03-24
 */
public interface CrmBannerService extends IService<CrmBanner> {

//    查询所有的banner
    List<CrmBanner> selectAllBanner();
}
