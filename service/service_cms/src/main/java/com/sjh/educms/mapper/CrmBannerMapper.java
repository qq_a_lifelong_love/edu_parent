package com.sjh.educms.mapper;

import com.sjh.educms.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author 宋军辉
 * @since 2023-03-24
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
