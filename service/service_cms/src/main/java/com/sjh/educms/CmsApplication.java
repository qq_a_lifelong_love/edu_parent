package com.sjh.educms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author: sjh
 * @Date: 2023-03-24
 * @Version: 1.0
 */
@SpringBootApplication
@ComponentScan("com.sjh")//指定扫描位置
@MapperScan("com.sjh.educms.mapper")
@EnableDiscoveryClient//服务调用
public class CmsApplication {

    //        swagger
//        http://localhost:8004/swagger-ui.html

    public static void main(String[] args) {
        SpringApplication.run(CmsApplication.class,args);
    }
}
