package com.sjh.educms.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sjh.commonutils.Result;
import com.sjh.educms.entity.CrmBanner;
import com.sjh.educms.service.CrmBannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 后台banner管理接口 后端控制器
 * </p>
 *
 * @author 宋军辉
 * @since 2023-03-24
 */
@Api(description = "网站首页Banner列表")
@RestController
@RequestMapping("/educms/banneradmin")
@CrossOrigin//解决跨域
public class BannerAdminController {

    @Autowired
    private CrmBannerService bannerService;

//    1分页查询banner
    @ApiOperation(value = "1分页查询banner")
    @GetMapping("/pageBanner/{page}/{limit}")
    public Result pageBanner(@PathVariable("page") long page,@PathVariable("limit") long limit){

        Page<CrmBanner> pageBanner = new Page<>(page,limit);
        bannerService.page(pageBanner,null);

        return Result.ok().data("items",pageBanner.getRecords()).data("total",pageBanner.getTotal());
    }

//    2添加banner
    @ApiOperation(value = "添加banner")
    @PostMapping("/addBanner")
    public Result addBanner(@RequestBody CrmBanner crmBanner){
        bannerService.save(crmBanner);
        return Result.ok();
    }


    @ApiOperation(value = "获取Banner")
    @GetMapping("/getBanner/{id}")
    public Result getBanner(@PathVariable String id){
        CrmBanner banner = bannerService.getById(id);
        return Result.ok().data("item",banner);
    }


    @ApiOperation(value = "修改banner")
    @PutMapping("/updateBanner")
    public Result updateBanner(@RequestBody CrmBanner banner){

        bannerService.updateById(banner);
        return Result.ok();
    }

    @ApiOperation(value = "删除Banner")
    @DeleteMapping("/deleteBanner")
    public Result deleteBanner(@PathVariable String id){
        bannerService.removeById(id);
        return Result.ok();
    }

}

