package com.sjh.aclservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sjh.aclservice.entity.UserRole;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author testjava
 * @since 2020-01-12
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
