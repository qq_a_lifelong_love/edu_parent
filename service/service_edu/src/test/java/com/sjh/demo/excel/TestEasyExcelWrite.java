package com.sjh.demo.excel;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.write.metadata.WriteSheet;
import io.swagger.annotations.Api;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: sjh
 * @Date: 2022-08-27
 * @Version: 1.0
 */
@Api(description = "实现写操作的两个方法,推荐是使用写法1")
public class TestEasyExcelWrite {
    public static void main(String[] args) {
//        写法 1 （实现最终的添加操作）
//        实现excel写的操作
//        1  设置写入文件夹地址和excel文件名称
        String filename = "E:\\写入的.xlsx";
//        2  调用easyexcel中的方法实现写操作
//        write()方法中的两个参数：第一个参数文件路径名称，第二个参数实体类class
        EasyExcel.write(filename, WriteEntity.class).sheet("写入的方法一").doWrite(getData());
    }

    //    创建方法返回list集合,//循环设置要添加的数据，最终封装到list集合中
    private static List<WriteEntity> getData() {
        List<WriteEntity> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            WriteEntity demoData = new WriteEntity();
            demoData.setSno(i);
            demoData.setSname("haha" + i);
            String m = (i % 2) == 0 ? "男" : "女";
            demoData.setSex(m);
            list.add(demoData);
        }
        return list;
    }
  /*
  public static void main(String[] args) throws Exception {
// 写法2，方法二需要手动关闭流
        String fileName = "E:\\写入2.xlsx";
// 这里 需要指定写用哪个class去写
        ExcelWriter excelWriter = EasyExcel.write(fileName, WriteEntity.class).build();
        WriteSheet writeSheet = EasyExcel.writerSheet("写入方法二").build();
        excelWriter.write(getData(), writeSheet);
/// 千万别忘记finish 会帮忙关闭流
        excelWriter.finish();
    }*/

}
