package com.sjh.demo.excelReadExample;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.Api;
import lombok.Data;

/**
 * @Author: sjh
 * @Date: 2022-08-28
 * @Version: 1.0
 */
@Api(description = "excel表读出的实体类")
@Data
public class ReadEntity {

    //    设置excel表头名称
    @ExcelProperty(value = "学生编号",index = 0)
    private Integer sno;

    @ExcelProperty(value = "学生姓名",index = 1)
    private String sname;

    @ExcelProperty(value = "性别",index = 2)
    private String sex;
}
