package com.sjh.demo.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.Api;
import lombok.Data;

/**
 * @Author: sjh
 * @Date: 2022-08-27
 * @Version: 1.0
 */
@Api(description = "创建一个写的实体类，需要和excel数据对应")
@Data
public class WriteEntity {

//    设置excel表头名称
    @ExcelProperty("学生编号")
    private Integer sno;

    @ExcelProperty("学生姓名")
    private String sname;

    @ExcelProperty("性别")
    private String sex;

}
