package com.sjh.demo.excelReadExample;

import com.alibaba.excel.EasyExcel;

/**
 * @Author: sjh
 * @Date: 2022-08-28
 * @Version: 1.0
 */
public class TestEasyExcelRead {
    public static void main(String[] args) {
//        实现excel的读操作
        String filename = "E:\\写入的.xlsx";
        EasyExcel.read(filename, ReadEntity.class, new ExcelListener()).sheet().doRead();

    }
}
