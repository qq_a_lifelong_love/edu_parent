package com.sjh.demo.excelReadExample;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;

import java.util.Map;

/**
 * @Author: sjh
 * @Date: 2022-08-28
 * @Version: 1.0
 */
public class ExcelListener extends AnalysisEventListener<ReadEntity> {

    //一行一行读取excel内容
    @Override
    public void invoke(ReadEntity data, AnalysisContext analysisContext) {
        System.out.println("*****" + data);
    }

    //读取表头内容

    @Override
    public void invokeHeadMap(Map<Integer, String> headMap, AnalysisContext context) {
        System.out.println("表头：" + headMap);

    }


    //读取完成之后
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) { }

}
