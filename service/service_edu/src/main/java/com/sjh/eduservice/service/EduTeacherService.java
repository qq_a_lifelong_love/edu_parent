package com.sjh.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sjh.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author 宋军辉
 * @since 2022-08-07
 */
public interface EduTeacherService extends IService<EduTeacher> {

    //1，分页查询讲师的方法
    Map<String, Object> getTeacherFrontList(Page<EduTeacher> pageTeacher);
}
