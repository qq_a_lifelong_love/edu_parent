package com.sjh.eduservice.service.impl;

import com.sjh.eduservice.entity.EduCourseDescription;
import com.sjh.eduservice.mapper.EduCourseDescriptionMapper;
import com.sjh.eduservice.service.EduCourseDescriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程简介 服务实现类
 * </p>
 *
 * @author 宋军辉
 * @since 2022-09-04
 */
@Service
public class EduCourseDescriptionServiceImpl extends ServiceImpl<EduCourseDescriptionMapper, EduCourseDescription> implements EduCourseDescriptionService {

}
