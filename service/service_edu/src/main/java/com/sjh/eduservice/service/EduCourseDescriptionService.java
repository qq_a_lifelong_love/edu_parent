package com.sjh.eduservice.service;

import com.sjh.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author 宋军辉
 * @since 2022-09-04
 */
public interface EduCourseDescriptionService extends IService<EduCourseDescription> {

}
