package com.sjh.eduservice.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sjh.eduservice.entity.EduSubject;
import com.sjh.eduservice.entity.excel.SubjectData;
import com.sjh.eduservice.service.EduSubjectService;
import com.sjh.servicebase.exceptionhandler.GuliException;
import javassist.NotFoundException;

/**
 * @Author: sjh
 * @Date: 2022-08-28
 * @Version: 1.0
 */
//创建监听器
public class SubjectExcelListener extends AnalysisEventListener<SubjectData> {

    //    因为SubjectExcelListener不能交给spring进行管理，需要自己new，不能注入其他对象
//    不能实现数据库操作
    public EduSubjectService eduSubjectService;

    public SubjectExcelListener() {
    }

    public SubjectExcelListener(EduSubjectService eduSubjectService) {
        this.eduSubjectService = eduSubjectService;
    }

    //    读取excel中的内容，一行一行进行读取
    @Override
    public void invoke(SubjectData subjectData, AnalysisContext analysisContext) {
        if (subjectData == null) {
            throw new GuliException(20001, "文件数据为空");
        }

//          因为一行一行读取，第一个值是一级分类，第二个值是二级分类
//          先判断一级分类是否重复
        EduSubject existOneSubject = this.existOneSubject(eduSubjectService, subjectData.getOneSubjectName());
        if (existOneSubject == null) {//没有一级分类，则进行添加一级分类
            existOneSubject = new EduSubject();
            existOneSubject.setParentId("0");
            existOneSubject.setTitle(subjectData.getOneSubjectName());//一级分类名称
            eduSubjectService.save(existOneSubject);
        }

//        获取一级分类的id值
        String pid = existOneSubject.getId();
//        添加二级分类
//        先判断二级分类是否重复
        EduSubject existTwoSubject = this.existTwoSubject(eduSubjectService, subjectData.getTwoSubjectName(), pid);
        if (existTwoSubject==null){//没有二级分类则进行添加二级分类
            existTwoSubject = new EduSubject();
            existTwoSubject.setParentId(pid);
            existTwoSubject.setTitle(subjectData.getTwoSubjectName());//二级分类名称
            eduSubjectService.save(existTwoSubject);
        }


    }

    //    判断一级分类不能重复添加
    private EduSubject existOneSubject(EduSubjectService eduSubjectService, String name) {
        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("title", name);
        wrapper.eq("parent_id", "0");
        EduSubject oneSubject = eduSubjectService.getOne(wrapper);
//        sql语句 select * from edu_subject where title=? and parent_id=0
        return oneSubject;
    }

    //    判断二级分类不能重复添加
    private EduSubject existTwoSubject(EduSubjectService eduSubjectService, String name, String pid) {
        QueryWrapper<EduSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("title", name);
        wrapper.eq("parent_id", pid);
        EduSubject twoSubject = eduSubjectService.getOne(wrapper);
        return twoSubject;
    }

    //    读取完成之后执行
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
