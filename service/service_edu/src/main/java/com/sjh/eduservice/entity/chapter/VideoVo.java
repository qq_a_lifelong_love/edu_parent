package com.sjh.eduservice.entity.chapter;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/29
 */
@Data
public class VideoVo {

    private String id;

    private String title;

    @ApiModelProperty(value = "视频id")
    private String videoCourseId;

}
