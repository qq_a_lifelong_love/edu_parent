package com.sjh.eduservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Author: sjh
 * @Date: 2022-08-07
 * @Version: 1.0
 */

@SpringBootApplication
@ComponentScan(basePackages = {"com.sjh"})
@EnableDiscoveryClient//nacos注册
@EnableFeignClients//Feign服务调用
public class EduApplication {
    public static void main(String[] args) {
        SpringApplication.run(EduApplication.class, args);

//        swagger
//        http://localhost:8001/swagger-ui.html
    }
}
