package com.sjh.eduservice.entity.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: sjh
 * @Date: 2023-03-22
 * @Version: 1.0
 */

@Data
public class CoursePublishVo {
    private String id;
    private String title;
    private String cover;

    @ApiModelProperty(value = "课时数")
    private String lessonNum;

    @ApiModelProperty(value = "一级分类")
    private String subjectLevelOne;

    @ApiModelProperty(value = "二级分类")
    private String subjectLevelTwo;

    @ApiModelProperty(value = "讲师名称")
    private String teacherName;

    @ApiModelProperty(value = "价格")
    private String price;//只用于显示
}
