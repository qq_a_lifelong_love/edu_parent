package com.sjh.eduservice.entity.subject;

import lombok.Data;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/1
 */
// 二级分类
@Data
public class TwoSubject {

    private String id;
    private String title;
}
