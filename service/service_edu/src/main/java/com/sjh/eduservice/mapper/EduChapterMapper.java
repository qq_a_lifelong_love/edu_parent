package com.sjh.eduservice.mapper;

import com.sjh.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author 宋军辉
 * @since 2022-09-04
 */
public interface EduChapterMapper extends BaseMapper<EduChapter> {

}
