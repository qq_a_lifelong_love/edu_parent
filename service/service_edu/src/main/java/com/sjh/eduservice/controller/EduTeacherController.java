package com.sjh.eduservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sjh.commonutils.Result;
import com.sjh.eduservice.entity.EduTeacher;
import com.sjh.eduservice.entity.vo.TeacherQuery;
import com.sjh.eduservice.service.EduTeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author 宋军辉
 * @since 2022-08-07
 */
@Api(description = "讲师管理")
@RestController
@RequestMapping("/eduservice/teacher")
@CrossOrigin //解决跨域
public class EduTeacherController {
    //    swagger启动界面
//  http://localhost:8001/swagger-ui.html
    @Autowired
    private EduTeacherService teacherService;


    @ApiOperation(value = "查询讲师表的所有数据")
    @GetMapping("/findAll")
    public Result findAllTeacher() {
        List<EduTeacher> list = teacherService.list(null);
        return Result.ok().data("items", list);

//        http://localhost:8080/eduservice/teacher/findAll
    }


    @ApiOperation(value = "逻辑删除根据id删除讲师")
    @DeleteMapping("{id}")
    public Result removeTeacher(@ApiParam(name = "id", value = "讲师id", required = true)
                                @PathVariable String id) {
        boolean flag = teacherService.removeById(id);
        if (flag) {
            return Result.ok();
        } else {

            return Result.error();
        }

    }

    /**
     * 3分页查询讲师的方法
     *
     * @param current 当前页
     * @param limit   每页的记录数
     * @return 讲师地分页列表
     */
    @ApiOperation(value = "分页查询讲师的方法")
    @GetMapping("/pageTeacher/{current}/{limit}")
    public Result pageListTeacher(@PathVariable long current,
                                  @PathVariable long limit) {

//        创建分页对象
        Page<EduTeacher> teacherPage = new Page<>(current, limit);

//        调用方法实现分页
//        调用方法时候，底层封装，把分页所有数据都封装到teacherPage中
        teacherService.page(teacherPage, null);
//      总记录数
        long total = teacherPage.getTotal();
//        数据list集合
        List<EduTeacher> records = teacherPage.getRecords();


//        Map map = new HashMap<>();
//        map.put("total",total);
//        map.put("rows",records);
//
//        return Result.ok().data(map);

        return Result.ok().data("total", total).data("rows", records);


    }


    @ApiOperation(value = "分页带条件查询讲师信息")
    @PostMapping("/pageTeacherCondition/{current}/{limit}")
    public Result pageTeacherCondition(@PathVariable long current,
                                       @PathVariable long limit,
                                       @RequestBody(required = false) TeacherQuery teacherQuery) {
//        创建分页对象
        Page<EduTeacher> pageTeacher = new Page<>(current, limit);
//        构建条件
        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
//        多条件组合查询
        //mybatis学过 动态sql
        String name = teacherQuery.getName();
        Integer level = teacherQuery.getLevel();
        String begin = teacherQuery.getBegin();
        String end = teacherQuery.getEnd();
        //判断条件值是否为空，如果不为空拼接条件
        if (!StringUtils.isEmpty(name)) {
            wrapper.like("name", name);
        }
        if (!StringUtils.isEmpty(level)) {
            wrapper.eq("level", level);
        }
        if (!StringUtils.isEmpty(begin)) {
            wrapper.ge("gmt_create", begin);
        }
        if (!StringUtils.isEmpty(end)) {
            wrapper.le("gmt_create", end);
        }

//        排序
        wrapper.orderByDesc("gmt_create");
//        调用方法实现分页查询分页
        teacherService.page(pageTeacher, wrapper);

        //      总记录数
        long total = pageTeacher.getTotal();
//        数据list集合
        List<EduTeacher> records = pageTeacher.getRecords();
        return Result.ok().data("total", total).data("rows", records);
    }

    @ApiOperation("添加讲师信息")
    @PostMapping("/addTeacher")
    public Result addTeacher(@RequestBody EduTeacher eduTeacher) {
        boolean save = teacherService.save(eduTeacher);
        if (save) {
            return Result.ok();
        } else {
            return Result.error();
        }
    }

    @ApiOperation(value = "根据讲师id进行查询")
    @GetMapping("/getTeacher/{id}")
    public Result getTeacher(@PathVariable String id) {
        EduTeacher eduTeacher = teacherService.getById(id);
        return Result.ok().data("teacher", eduTeacher);
    }

    @ApiOperation(value = "讲师修改功能|")
    @PostMapping("/updateTeacher")
    public Result updateTeacher(@RequestBody EduTeacher eduTeacher) {
        boolean flag = teacherService.updateById(eduTeacher);
        if (flag) {
            return Result.ok();
        } else {
            return Result.error();
        }
    }
}

