package com.sjh.eduservice.controller.front;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sjh.commonutils.JwtUtils;
import com.sjh.commonutils.Result;
import com.sjh.commonutils.ordervo.CourseWebVoOrder;
import com.sjh.eduservice.client.OrdersClient;
import com.sjh.eduservice.entity.EduCourse;
import com.sjh.eduservice.entity.chapter.ChapterVo;
import com.sjh.eduservice.entity.frontvo.CourseFrontVo;
import com.sjh.eduservice.entity.frontvo.CourseWebVo;
import com.sjh.eduservice.service.EduChapterService;
import com.sjh.eduservice.service.EduCourseService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/eduservice/coursefront")
//@CrossOrigin
public class CourseFrontController {

    @Autowired
    private EduCourseService courseService;

    @Autowired
    private EduChapterService chapterService;

    @Autowired
    private OrdersClient ordersClient;

    //1，条件查询分页查询课程（前台）
    @ApiOperation(value = "条件查询分页查询课程（前台）")
    @PostMapping("getFrontCourseList/{page}/{limit}")
    public Result getFrontCourseList(@PathVariable long page,
                                     @PathVariable long limit,
                                     @RequestBody(required = false) CourseFrontVo courseFrontVo){

        Page<EduCourse> pageCourse = new Page<>(page,limit);
        Map<String,Object> map = courseService.getCourseFrontList(pageCourse,courseFrontVo);
        return Result.ok().data(map);
    }

    //2，课程详情的方法
    @ApiOperation(value = "课程详情的方法")
    @GetMapping("getFrontCourseInfo/{courseId}")
    public Result getFrontCourseInfo(@PathVariable String courseId, HttpServletRequest request){
        //根据课程id，编写sql语句查询课程信息
        CourseWebVo courseWebVo = courseService.getBaseCourseInfo(courseId);

        //根据课程id查询章节和小节
        List<ChapterVo> chapterVideoList = chapterService.getChapterVideoByCourseId(courseId);

        //根据课程id和用户id查询当前课程是否已经支付过了
        String token = JwtUtils.getMemberIdByJwtToken(request);
        boolean buyCourse = ordersClient.isBuyCourse(courseId,token);

        return Result.ok().data("courseWebVo",courseWebVo)
                .data("chapterVideoList",chapterVideoList).data("isBuy",buyCourse);
    }

    //3，根据课程id查询课程的信息
    @PostMapping("/getCourseInfoOrder/{id}")
    public CourseWebVoOrder getCourseInfoOrder(@PathVariable String id){
        CourseWebVo courseInfo = courseService.getBaseCourseInfo(id);
        CourseWebVoOrder courseWebVoOrder = new CourseWebVoOrder();
        BeanUtils.copyProperties(courseInfo,courseWebVoOrder);
        return courseWebVoOrder;
    }
}
