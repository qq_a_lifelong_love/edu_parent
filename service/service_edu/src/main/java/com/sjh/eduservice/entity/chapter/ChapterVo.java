package com.sjh.eduservice.entity.chapter;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/29
 */
@Data
public class ChapterVo {

    private String id;

    private String title;

    //    章节里边有很多小节，也是一对多的关系
    private List<VideoVo> children = new ArrayList<>();

}
