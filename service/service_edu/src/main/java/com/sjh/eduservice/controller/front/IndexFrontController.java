package com.sjh.eduservice.controller.front;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sjh.commonutils.Result;
import com.sjh.eduservice.entity.EduCourse;
import com.sjh.eduservice.entity.EduTeacher;
import com.sjh.eduservice.service.EduCourseService;
import com.sjh.eduservice.service.EduTeacherService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: sjh
 * @Date: 2023-03-24
 * @Version: 1.0
 */
@RestController
@RequestMapping("/eduservice/indexfront")
@CrossOrigin//解决跨域
public class IndexFrontController {

    @Autowired
    private EduCourseService courseService;

    @Autowired
    private EduTeacherService teacherService;

//    查询前8条热门课程，查询前4条名师
    @ApiOperation(value = "查询前8条热门课程，查询前4条名师")
    @GetMapping("/index")
    public Result index(){
//        查询前8条热门课程
        QueryWrapper<EduCourse> wrapper=new QueryWrapper<>();
        wrapper.orderByDesc("id");
        wrapper.last("limit 8");

        List<EduCourse> courseList = courseService.list(wrapper);

//        查询前4条名师
        QueryWrapper<EduTeacher> teacherQueryWrapper=new QueryWrapper<>();
        teacherQueryWrapper.orderByDesc("id");
        teacherQueryWrapper.last("limit 4");
        List<EduTeacher> teacherList = teacherService.list(teacherQueryWrapper);


        return Result.ok().data("courseList",courseList).data("teacherList",teacherList);
    }
}
