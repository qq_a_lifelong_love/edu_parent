package com.sjh.eduservice.controller;


import com.sjh.commonutils.Result;
import com.sjh.eduservice.entity.EduChapter;
import com.sjh.eduservice.entity.chapter.ChapterVo;
import com.sjh.eduservice.service.EduChapterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author 宋军辉
 * @since 2022-09-04
 */
@Api(description = "章节列表信息")
@RestController
@RequestMapping("/eduservice/edu-chapter")
@CrossOrigin//解决跨域问题
public class EduChapterController {

    @Autowired
    private EduChapterService chapterService;

//    课程大纲列表 根据课程id进行查询
    @GetMapping("/getChapterVideo/{courseId}")
    public Result getChapterVideo(@PathVariable String courseId){
        List<ChapterVo> list = chapterService.getChapterVideoByCourseId(courseId);

        return Result.ok().data("allChapterVideo",list);
    }
//    添加章节
    @ApiModelProperty(value = "添加章节")
    @PostMapping("/addChapter")
    public Result addChapter(@RequestBody EduChapter eduChapter){
        chapterService.save(eduChapter);
        return Result.ok();
    }

//    修改就是下方先查询在修改的两个方法
//    根据章节id查询章节信息
    @ApiModelProperty(value = "根据章节查询章节信息")
    @PostMapping("/getChapterInfo/{chapterId}")
    public Result getChapterInfo(@PathVariable String chapterId){
        EduChapter eduChapter = chapterService.getById(chapterId);
        return Result.ok().data("chapter",eduChapter);
    }
//    修改章节
    @ApiModelProperty(value = "修改章节")
    @PostMapping("/updateChapter")
    public Result updateChapter(@RequestBody EduChapter eduChapter){
        chapterService.updateById(eduChapter);
        return Result.ok();
    }

//    删除章节
    @ApiModelProperty(value = "删除章节")
    @DeleteMapping("/deleteChapter")
    public Result deleteChapter(@PathVariable String chapterId){
        Boolean flag = chapterService.deleteChapter(chapterId);
        if (flag){
            return Result.ok();
        }else {
            return Result.error();
        }

    }

}

