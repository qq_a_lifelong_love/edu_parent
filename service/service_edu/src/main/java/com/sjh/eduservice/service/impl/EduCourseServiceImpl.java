package com.sjh.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sjh.eduservice.entity.EduCourse;
import com.sjh.eduservice.entity.EduCourseDescription;
import com.sjh.eduservice.entity.frontvo.CourseFrontVo;
import com.sjh.eduservice.entity.frontvo.CourseWebVo;
import com.sjh.eduservice.entity.vo.CourseInfoVO;
import com.sjh.eduservice.entity.vo.CoursePublishVo;
import com.sjh.eduservice.mapper.EduCourseMapper;
import com.sjh.eduservice.service.EduChapterService;
import com.sjh.eduservice.service.EduCourseDescriptionService;
import com.sjh.eduservice.service.EduCourseService;
import com.sjh.eduservice.service.EduVideoService;
import com.sjh.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author 宋军辉
 * @since 2022-09-04
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {

    @Autowired
    private EduCourseService courseService;

    //    注入小节和章节的service
    @Autowired
    private EduVideoService eduVideoService;

    @Autowired
    private EduChapterService eduChapterService;

    //    课程描述的注入
    @Autowired
    private EduCourseDescriptionService courseDescriptionService;

    //    添加课程基本信息的方法
//    @Transactional(rollbackFor = Exception.class)
    @Override
    public String addCourseInfo(CourseInfoVO courseInfoVO) {
//      1.向课程表中添加课程基本信息
//        将CourseInfoVO对象转换成eduCourse对象
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVO, eduCourse);
        int insert = baseMapper.insert(eduCourse);
        if (insert == 0) {//添加失败
            throw new GuliException(20001, "添加课程信息失败");
        }

//        获取到添加之后课程id
        String cid = eduCourse.getId();
//      2.向课程简介表中添加课程简介
//        edu_course_description
        EduCourseDescription courseDescription = new EduCourseDescription();
        courseDescription.setDescription(courseInfoVO.getDescription());
//        设置描述的id就是课程的id
        courseDescription.setId(cid);
        courseDescriptionService.save(courseDescription);

//        返回id
        return cid;


    }

    //    根据课程查询课程基本信息
    @Override
    public CourseInfoVO getCourseInfo(String courseId) {
//        1先查询课程表
        EduCourse eduCourse = baseMapper.selectById(courseId);
        CourseInfoVO courseInfoVO = new CourseInfoVO();
        BeanUtils.copyProperties(eduCourse, courseInfoVO);

//        2查询描述表
        EduCourseDescription courseDescription = courseDescriptionService.getById(courseId);
        courseInfoVO.setDescription(courseDescription.getDescription());

        return courseInfoVO;
    }

    //    修改课程信息
    @Override
    public void updateCourseInfo(CourseInfoVO courseInfoVO) {
//        1先去修改课程表
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(courseInfoVO, eduCourse);
        int update = baseMapper.updateById(eduCourse);
        if (update == 0) {
            throw new GuliException(20001, "修改课程信息失败");
        }
//        2修改描述表
        EduCourseDescription description = new EduCourseDescription();
        description.setId(courseInfoVO.getId());
        description.setDescription(courseInfoVO.getDescription());
        courseDescriptionService.updateById(description);
    }

    //    根据课程id查询课程确认信息
    @Override
    public CoursePublishVo publishCourseInfo(String id) {
        //调用mapper
        CoursePublishVo publishCourseInfo = baseMapper.getPublishCourseInfo(id);
        return publishCourseInfo;
    }

    //    删除课程
    @Override
    public void removeCourse(String courseId) {
//        1根据课程id删除小节
        eduVideoService.removeVideoByCourseId(courseId);

//        2根据课程id删除章节
        eduChapterService.removeChapterByCourseId(courseId);

//        3根据课程id删除描述
        courseDescriptionService.removeById(courseId);

//        4根据课程id删除课程本身
        int result = baseMapper.deleteById(courseId);

        if (result == 0) {
            throw new GuliException(200001, "删除课程失败");
        }

    }

    //1，条件查询分页查询课程（前台）
    @Override
    public Map<String, Object> getCourseFrontList(Page<EduCourse> pageCourse, CourseFrontVo courseFrontVo) {
        //根据讲师id查询所讲课程

        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        //判断条件值是否为空，不为空拼接
        if (!StringUtils.isEmpty(courseFrontVo.getSubjectParentId())) { //一级分类
            wrapper.eq("subject_parent_id", courseFrontVo.getSubjectParentId());
        }
        if (!StringUtils.isEmpty(courseFrontVo.getSubjectId())) { //二级分类
            wrapper.eq("subject_id", courseFrontVo.getSubjectId());
        }
        if (!StringUtils.isEmpty(courseFrontVo.getBuyCountSort())) { //关注度
            wrapper.orderByDesc("buy_count");
        }
        if (!StringUtils.isEmpty(courseFrontVo.getGmtCreateSort())) { //最新
            wrapper.orderByDesc("gmt_create");
        }

        if (!StringUtils.isEmpty(courseFrontVo.getPriceSort())) {//价格
            wrapper.orderByDesc("price");
        }

        baseMapper.selectPage(pageCourse, wrapper);

        List<EduCourse> records = pageCourse.getRecords();
        long current = pageCourse.getCurrent();
        long pages = pageCourse.getPages();
        long size = pageCourse.getSize();
        long total = pageCourse.getTotal();
        boolean hasNext = pageCourse.hasNext();//下一页
        boolean hasPrevious = pageCourse.hasPrevious();//上一页

        //把分页数据获取出来，放到map集合
        Map<String, Object> map = new HashMap<>();
        map.put("items", records);
        map.put("current", current);
        map.put("pages", pages);
        map.put("size", size);
        map.put("total", total);
        map.put("hasNext", hasNext);
        map.put("hasPrevious", hasPrevious);

        //map返回
        return map;
    }

    @Override
    public CourseWebVo getBaseCourseInfo(String courseId) {
        return baseMapper.getBaseCourseInfo(courseId);
    }


}
