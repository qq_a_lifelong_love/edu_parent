package com.sjh.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sjh.eduservice.entity.EduChapter;
import com.sjh.eduservice.entity.EduVideo;
import com.sjh.eduservice.entity.chapter.ChapterVo;
import com.sjh.eduservice.entity.chapter.VideoVo;
import com.sjh.eduservice.mapper.EduChapterMapper;
import com.sjh.eduservice.service.EduChapterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sjh.eduservice.service.EduVideoService;
import com.sjh.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author 宋军辉
 * @since 2022-09-04
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {

    //    注入小节的service
    @Autowired
    private EduVideoService videoService;

    //    课程大纲列表 根据课程id进行查询
    @Override
    public List<ChapterVo> getChapterVideoByCourseId(String courseId) {


//    1：创建课程id查询课程里面所有的章节
        QueryWrapper<EduChapter> wrapperChapter = new QueryWrapper<>();
        wrapperChapter.eq("course_id", courseId);
        List<EduChapter> eduChapterList = baseMapper.selectList(wrapperChapter);

//    2：根据课程id查询课程里面所有的小节
        QueryWrapper<EduVideo> wrapperVideo = new QueryWrapper<>();
        wrapperVideo.eq("course_id", courseId);
        List<EduVideo> eduVideoList = videoService.list(wrapperVideo);

//创建一个集合用于封装最终的数据信息
        List<ChapterVo> finalList = new ArrayList<>();

//    3：遍历查询章节list集合，进行封装
//        遍历查询章节list集合
        for (int i = 0; i < eduChapterList.size(); i++) {
//            获取每个章节
            EduChapter eduChapter = eduChapterList.get(i);
//            eduChapter对象复制到ChapterVo里面
            ChapterVo chapterVo = new ChapterVo();
            BeanUtils.copyProperties(eduChapter, chapterVo);
//            把chapterVo放到最终list集合
            finalList.add(chapterVo);

//            创建集合，用于封装章节的小节
            List<VideoVo> videoList = new ArrayList<>();
//      4：遍历查询小节list集合，进行封装
            for (int m = 0; m < eduVideoList.size(); m++) {
//                得到每个小节
                EduVideo eduVideo = eduVideoList.get(m);
//                进行判断：小节里面的chapterId和章节里面的id是一样的
                if (eduVideo.getChapterId().equals(eduChapter.getId())) {
//                    进行封装
                    VideoVo videoVo = new VideoVo();
                    BeanUtils.copyProperties(eduVideo, videoVo);
//                    放到小节封装集合中
                    videoList.add(videoVo);
                }

            }
//            把封装之后小节list集合，放到章节对象里面
            chapterVo.setChildren(videoList);

        }
        return finalList;
    }

    //    删除章节的方法
    @Override
    public Boolean deleteChapter(String chapterId) {
//        根据chapterId章节id查询小节表，如果查询有数据，不进行删除
        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("chapter_id",chapterId);
        int count = videoService.count(wrapper);
        if (count>0){//证明章节里边有小节，则不进行删除章节
            throw new GuliException(20001,"章节内有小节存在，不能删除该章节");
        }else {//证明章节里边没有小节，可以直接删除章节
//            执行删除章节
            int result = baseMapper.deleteById(chapterId);
//            成功 1>0     0>0
            return result>0;
        }
    }

    //        2根据课程id删除章节
    @Override
    public void removeChapterByCourseId(String courseId) {

        QueryWrapper<EduChapter> wrapper=new QueryWrapper<>();
        wrapper.eq("course_id",courseId);
        baseMapper.delete(wrapper);
    }

}
