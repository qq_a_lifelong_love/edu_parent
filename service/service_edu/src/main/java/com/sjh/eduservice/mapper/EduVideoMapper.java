package com.sjh.eduservice.mapper;

import com.sjh.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author 宋军辉
 * @since 2022-09-04
 */
public interface EduVideoMapper extends BaseMapper<EduVideo> {

}
