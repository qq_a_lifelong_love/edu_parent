package com.sjh.eduservice.controller;

import com.sjh.commonutils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: sjh
 * @Date: 2022-08-14
 * @Version: 1.0
 */

@Api(description = "登录")
@RestController
@RequestMapping("/eduservice/user")
@CrossOrigin  //解决跨域
public class EduLoginController {

    @ApiOperation(value = "登录")
    @RequestMapping("/login")
    public Result login(){
        return Result.ok().data("token","admin");
    }

    @ApiOperation(value = "查询登录信息")
    @RequestMapping("/info")
    public Result info(){
        return Result.ok().data("roles","[admin]").data("name","admin").data("avatar","https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
    }
}
