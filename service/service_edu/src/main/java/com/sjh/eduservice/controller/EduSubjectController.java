package com.sjh.eduservice.controller;


import com.sjh.commonutils.Result;
import com.sjh.eduservice.entity.subject.OneSubject;
import com.sjh.eduservice.service.EduSubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author 宋军辉
 * @since 2022-08-28
 */
@Api(description = "添加课程分类功能")
@RestController
@RequestMapping("/eduservice/subject")
@CrossOrigin//解决跨域问题
public class EduSubjectController {
//  http://localhost:8001/swagger-ui.html

    @Autowired
    private EduSubjectService eduSubjectService;

    //    添加课程分类
//    获取上传过来的文件，把文件内容读取出来
    @ApiOperation(value = "导入excel文件")
    @PostMapping("/addSubject")
    public Result addSubject(MultipartFile file) {
//        上传过来的excel文件
        eduSubjectService.saveSubject(file, eduSubjectService);

        return Result.ok();
    }

    //    课程分类列表（树形结构）
    @ApiOperation(value = "课程分类列表（树形）")
    @GetMapping("/getAllSubject")
    public Result getAllSubject() {
//        list集合泛型是一级分类    里边自动包含二级分类
        List<OneSubject> list = eduSubjectService.getAllOneTwoSubject();

        return Result.ok().data("list", list);
    }


}

