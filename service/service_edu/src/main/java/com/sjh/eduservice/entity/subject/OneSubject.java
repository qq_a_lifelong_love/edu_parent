package com.sjh.eduservice.entity.subject;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author : 宋军辉
 * @Date : 2022/9/1
 */
// 一级分类
@Data
public class OneSubject {
    private String id;
    private String title;

    //    一个一级分类中有多个二级分类
    private List<TwoSubject> children = new ArrayList<>();

}
