package com.sjh.eduservice.service;

import com.sjh.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author 宋军辉
 * @since 2022-09-04
 */
public interface EduVideoService extends IService<EduVideo> {

    //        1根据课程id删除小节
    void removeVideoByCourseId(String courseId);
}
