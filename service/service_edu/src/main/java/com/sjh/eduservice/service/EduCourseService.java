package com.sjh.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.sjh.eduservice.entity.EduCourse;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sjh.eduservice.entity.frontvo.CourseFrontVo;
import com.sjh.eduservice.entity.frontvo.CourseWebVo;
import com.sjh.eduservice.entity.vo.CourseInfoVO;
import com.sjh.eduservice.entity.vo.CoursePublishVo;

import java.util.Map;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author 宋军辉
 * @since 2022-09-04
 */
public interface EduCourseService extends IService<EduCourse> {

    //    添加课程基本信息的方法
    String addCourseInfo(CourseInfoVO courseInfoVO);

//    根据课程查询课程基本信息
    CourseInfoVO getCourseInfo(String courseId);

//    修改课程信息
    void updateCourseInfo(CourseInfoVO courseInfoVO);

//    根据课程id查询课程确认信息
    CoursePublishVo publishCourseInfo(String id);

    //    删除课程
    void removeCourse(String courseId);

    //1，条件查询分页查询课程（前台）
    Map<String, Object> getCourseFrontList(Page<EduCourse> pageCourse, CourseFrontVo courseFrontVo);

    //根据课程id，编写sql语句查询课程信息
    CourseWebVo getBaseCourseInfo(String courseId);
}
