package com.sjh.eduservice.mapper;

import com.sjh.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程科目 Mapper 接口
 * </p>
 *
 * @author 宋军辉
 * @since 2022-08-28
 */
public interface EduSubjectMapper extends BaseMapper<EduSubject> {

}
