package com.sjh.eduservice.service;

import com.sjh.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sjh.eduservice.entity.chapter.ChapterVo;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author 宋军辉
 * @since 2022-09-04
 */
public interface EduChapterService extends IService<EduChapter> {

    //    课程大纲列表 根据课程id进行查询
    List<ChapterVo> getChapterVideoByCourseId(String courseId);

//    删除章节的方法
    Boolean deleteChapter(String chapterId);

//        2根据课程id删除章节
    void removeChapterByCourseId(String courseId);
}
