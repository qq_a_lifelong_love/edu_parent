package com.sjh.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sjh.eduservice.client.VodClient;
import com.sjh.eduservice.entity.EduVideo;
import com.sjh.eduservice.mapper.EduVideoMapper;
import com.sjh.eduservice.service.EduVideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author 宋军辉
 * @since 2022-09-04
 */
@Service
public class EduVideoServiceImpl extends ServiceImpl<EduVideoMapper, EduVideo> implements EduVideoService {

    //    注入vodClient
    @Autowired
    private VodClient vodClient;

    //        1根据课程id删除小节
    @Override
    public void removeVideoByCourseId(String courseId) {
//        根据课程id查询课程里边所有的视频id
        QueryWrapper<EduVideo> wrapperVideo = new QueryWrapper<>();
        wrapperVideo.eq("course_id", courseId);
        wrapperVideo.select("video_source_id");//取出一列数据
        List<EduVideo> eduVideoList = baseMapper.selectList(wrapperVideo);
//        List<EduVideo>中的值变成List<String>
        List<String> videoIds = new ArrayList<>();
        for (int i = 0; i < eduVideoList.size(); i++) {
            EduVideo eduVideo = eduVideoList.get(i);
            String videoSourceId = eduVideo.getVideoSourceId();
            if (!StringUtils.isEmpty(videoSourceId)) {
                //            放到videoIds
                videoIds.add(videoSourceId);
            }

        }
//        多个视频id删除多个视频
        if (videoIds.size()>0){
            vodClient.deleteBatch(videoIds);
        }

        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id",courseId);
        baseMapper.delete(wrapper);
    }
}
