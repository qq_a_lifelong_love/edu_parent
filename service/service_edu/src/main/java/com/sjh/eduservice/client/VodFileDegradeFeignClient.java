package com.sjh.eduservice.client;

import com.sjh.commonutils.Result;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author: sjh
 * @Date: 2023-03-24
 * @Version: 1.0
 */

@Component
public class VodFileDegradeFeignClient implements VodClient{
    @Override
    public Result removeAlyVideo(String id) {
        return Result.error().message("time out:删除视频出错了");
    }

    @Override
    public Result deleteBatch(List<String> videoIdList) {
        return Result.error().message("time out:删除多个视频出错了");
    }
}
