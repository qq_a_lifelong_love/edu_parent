package com.sjh.eduservice.service;

import com.sjh.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.sjh.eduservice.entity.subject.OneSubject;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author 宋军辉
 * @since 2022-08-28
 */
public interface EduSubjectService extends IService<EduSubject> {

    //    添加课程分类
    void saveSubject(MultipartFile file, EduSubjectService eduSubjectService);



    //课程分类列表（树形）
    List<OneSubject> getAllOneTwoSubject();
}
