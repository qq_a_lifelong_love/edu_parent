package com.sjh.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.sjh.eduservice.entity.EduSubject;
import com.sjh.eduservice.entity.excel.SubjectData;
import com.sjh.eduservice.entity.subject.OneSubject;
import com.sjh.eduservice.entity.subject.TwoSubject;
import com.sjh.eduservice.listener.SubjectExcelListener;
import com.sjh.eduservice.mapper.EduSubjectMapper;
import com.sjh.eduservice.service.EduSubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author 宋军辉
 * @since 2022-08-28
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    //    添加课程分类
    @Override
    public void saveSubject(MultipartFile file, EduSubjectService eduSubjectService) {

        try {

//            得到文件输入流
            InputStream in = file.getInputStream();
//          调用方法进行读取
            EasyExcel.read(in, SubjectData.class, new SubjectExcelListener(eduSubjectService)).sheet().doRead();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<OneSubject> getAllOneTwoSubject() {
//        1 查询所有的一级分类 parentId = 0
        QueryWrapper<EduSubject> wrapperOne = new QueryWrapper<>();
        wrapperOne.eq("parent_id", "0");
//        List<EduSubject> list = this.list(wrapperOne);//和下边代码功能一样
        List<EduSubject> oneSubjectList = baseMapper.selectList(wrapperOne);

//        2 查询所有的二级分类 parentId != 0
        QueryWrapper<EduSubject> wrapperTwo = new QueryWrapper<>();
        wrapperOne.ne("parent_id", "0");
//        List<EduSubject> list = this.list(wrapperTwo);//和下边代码功能一样
        List<EduSubject> twoSubjectList = baseMapper.selectList(wrapperTwo);

//        创建list集合，用于存储最终封装数据
        ArrayList<OneSubject> finalSubjectList = new ArrayList<>();

//        3  封装一级分类
//        查询出来的一级分类的list集合遍历，得到每个一级分类对象，获取每个一级分类对象值，
//        封装到要求的list集合里面ArrayList<OneSubject> finalSubjectList
        for (int i = 0; i < oneSubjectList.size(); i++) {//遍历每个一级分类对象值
//            得到oneSubjectList每个EduSubject对象
            EduSubject eduSubject = oneSubjectList.get(i);

//            把eduSubject里面的值取出来，都放到oneSubject对象里面
            OneSubject oneSubject = new OneSubject();
//            oneSubject.setId(eduSubject.getId());
//            oneSubject.setTitle(eduSubject.getTitle());

//            //和上边的两行的set/get功能一样，用了工具类更加方便快捷
//            eduSubject中的值赋值到对应的oneSubject对象里边去
            BeanUtils.copyProperties(eduSubject, oneSubject);
//            多个OneSubject放到finalSubjectList里面
            finalSubjectList.add(oneSubject);


//            在一级分类循环遍历中查询所有的二级分类
//            创建list集合 封装每个一级分类的二级分类
            List<TwoSubject> twoFinalSubjectList = new ArrayList<>();
//            遍历二级分类的list集合
            for (int j = 0; j < twoSubjectList.size(); j++) {
//                获取每个二级分类
                EduSubject tSubject = twoSubjectList.get(j);
//                判断二级分类的parentId和一级分类的id是否一样
                if (tSubject.getParentId().equals(eduSubject.getId())) {
//                    把tSubject值复制到TwoSubject 里面，放到twoFinalSubjectList里面
                    TwoSubject twoSubject = new TwoSubject();

                    BeanUtils.copyProperties(tSubject, twoSubject);
//                    多个twoSubject放到 twoFinalSubjectList 里面
                    twoFinalSubjectList.add(twoSubject);
                }
            }
//            把一级下面所有的二级分类放到一级分类里面
            oneSubject.setChildren(twoFinalSubjectList);
        }

//        4  封装二级分类
        return finalSubjectList;
    }


}
