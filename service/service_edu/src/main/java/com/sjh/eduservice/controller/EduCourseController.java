package com.sjh.eduservice.controller;


import com.sjh.commonutils.Result;
import com.sjh.eduservice.entity.EduCourse;
import com.sjh.eduservice.entity.vo.CourseInfoVO;
import com.sjh.eduservice.entity.vo.CoursePublishVo;
import com.sjh.eduservice.service.EduCourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.ResultSet;
import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author 宋军辉
 * @since 2022-09-04
 */
@Api(description = "课程管理")
@RestController
@RequestMapping("/eduservice/course")
@CrossOrigin
public class EduCourseController {
    @Autowired
    private EduCourseService courseService;

    //    课程列表 基本实现
//    TODO 完善条件查询带分页
    @GetMapping("/getCourseList")
    public Result getCourseList() {
        List<EduCourse> list = courseService.list(null);
        return Result.ok().data("list", list);
    }

    //    添加课程基本信息的方法
    @ApiModelProperty(value = "添加课程基本信息")
    @PostMapping("/addCourseInfo")
    public Result addCourseInfo(@RequestBody CourseInfoVO courseInfoVO) {
//        返回添加之后课程id，未来后边添加大纲使用
        String id = courseService.addCourseInfo(courseInfoVO);
        return Result.ok().data("courseId", id);
    }


    //  根据课程查询课程基本信息
    @ApiModelProperty(value = "根据课程查询课程基本信息")
    @GetMapping("/getCourseInfo/{courseId}")
    public Result getCourseInfo(@PathVariable String courseId) {
        CourseInfoVO courseInfoVO = courseService.getCourseInfo(courseId);
        return Result.ok().data("courseInfoVo", courseInfoVO);
    }

    //    修改课程信息
    @ApiModelProperty(value = "修改课程信息")
    @PostMapping("/updateCourseInfo")
    public Result updateCourseInfo(@RequestBody CourseInfoVO courseInfoVO) {
        courseService.updateCourseInfo(courseInfoVO);
        return Result.ok();
    }

    //    根据课程id查询课程确认信息
    @ApiModelProperty(value = "根据课程id查询课程确认信息")
    @GetMapping("/getPublishCourseInfo/{id}")
    public Result getPublishCourseInfo(@PathVariable String id) {

        CoursePublishVo coursePublishVo = courseService.publishCourseInfo(id);
        return Result.ok().data("publishCourse", coursePublishVo);
    }

    //    课程的最终发布
//    修改课程状态    Draft未发布  Normal已发布
    @ApiModelProperty(value = "课程发布就是更改课程的状态")
    @PostMapping("/publishCourse/{id}")
    public Result publishCourse(@PathVariable String id) {

        EduCourse eduCourse = new EduCourse();
        eduCourse.setId(id);
        eduCourse.setStatus("Normal");
        courseService.updateById(eduCourse);
        return Result.ok();
    }

//    删除课程
    @ApiModelProperty(value = "删除课程")
    @DeleteMapping("/deleteCourse/{courseId}")
    public Result deleteCourse(@PathVariable String courseId){

        courseService.removeCourse(courseId);
        return Result.ok();
    }

}

