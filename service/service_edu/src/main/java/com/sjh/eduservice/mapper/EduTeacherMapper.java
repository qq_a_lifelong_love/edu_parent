package com.sjh.eduservice.mapper;

import com.sjh.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author 宋军辉
 * @since 2022-08-07
 */
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
