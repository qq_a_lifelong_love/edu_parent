package com.sjh.eduservice.controller;


import com.sjh.commonutils.Result;
import com.sjh.eduservice.client.VodClient;
import com.sjh.eduservice.entity.EduVideo;
import com.sjh.eduservice.service.EduVideoService;
import com.sjh.servicebase.exceptionhandler.GuliException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author 宋军辉
 * @since 2022-09-04
 */
@Api(description = "小节")
@RestController
@RequestMapping("/eduservice/video")
@CrossOrigin//解决跨越
public class EduVideoController {

    @Autowired
    private EduVideoService videoService;

//    注入vodClient
    @Autowired
    private VodClient vodClient;

//    添加小节
    @ApiModelProperty(value = "添加小节")
    @PostMapping("/addVideo")
    public Result addVideo(@RequestBody EduVideo eduVideo){

        videoService.save(eduVideo);
        return Result.ok();
    }
//    删除小节
    @ApiModelProperty(value = "删除小节")
    @DeleteMapping("{id}")
    public Result deleteVideo(@PathVariable String id){

//      根据小节id获取视频id，调用方法实现视频删除
        EduVideo eduVideo = videoService.getById(id);
        String videoSourceId = eduVideo.getVideoSourceId();
//        判断小节里面是否有视频id
        if (!StringUtils.isEmpty(videoSourceId)){
//        调用vodClient中的方法进行删除
//        根据视频id，远程调用实现视频的删除
            Result result = vodClient.removeAlyVideo(videoSourceId);
            if (result.getCode()==20001){
                throw new GuliException(20001,"删除视频失败，熔断器。。。。。。");
            }
        }

//      删除小节
        videoService.removeById(id);
        return Result.ok();
    }

//    TODO
//    修改小节
}

