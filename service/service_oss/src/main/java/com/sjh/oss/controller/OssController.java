package com.sjh.oss.controller;

import com.sjh.commonutils.Result;
import com.sjh.oss.service.OssService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Author: sjh
 * @Date: 2022-08-27
 * @Version: 1.0
 */
@Api(description = "文件上传")
@RestController
@RequestMapping("/eduoss/fileoss")
@CrossOrigin//解决跨域问题的注解
public class OssController {


    @Autowired
    private OssService ossService;

    //    上传头像方法
    @PostMapping
    public Result uploadOssFile(MultipartFile file) {
//        先获取上传文件MultipartFile
//        返回上传到oss的路径
        String url = ossService.uploadFileAvatar(file);

        return Result.ok().data("url", url);
    }

}
