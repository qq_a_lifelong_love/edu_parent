package com.sjh.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @Author: sjh
 * @Date: 2022-08-27
 * @Version: 1.0
 */
public interface OssService {
//上传头像到oss
    String uploadFileAvatar(MultipartFile file);
}
