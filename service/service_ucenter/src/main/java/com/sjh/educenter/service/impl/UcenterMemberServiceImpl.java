package com.sjh.educenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sjh.commonutils.JwtUtils;
import com.sjh.commonutils.MD5;
import com.sjh.educenter.entity.UcenterMember;
import com.sjh.educenter.entity.vo.RegisterVo;
import com.sjh.educenter.mapper.UcenterMemberMapper;
import com.sjh.educenter.service.UcenterMemberService;
import com.sjh.servicebase.exceptionhandler.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author 宋军辉
 * @since 2023-03-26
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {


    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    //    登录的方法
    @Override
    public String login(UcenterMember member) {

        //获取登录手机号和密码
        String mobile = member.getMobile();
        String password = member.getPassword();

        //手机号和密码非空判断
        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)) {
            throw new GuliException(20001, "登录失败");
        }

        //判断手机号是否正确
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", mobile);
        UcenterMember ucenterMember = baseMapper.selectOne(wrapper);

        //查询对象是否为空
        if (ucenterMember == null) {
            throw new GuliException(20001, "登录失败");
        }

        //判断密码
        /**
         *  因为存储到数据库中的密码是加密的，把输入的密码进行加密，再和数据库密码进行比较，加密方式MD5
         */
        if (!MD5.encrypt(password).equals(ucenterMember.getPassword())) {
            throw new GuliException(20001, "登录失败");
        }

        //判断用户是否禁用
        if (ucenterMember.getIsDeleted()) {
            throw new GuliException(20001, "登录失败");
        }

        //登录成功
        //生成token字符串，使用jwt工具
        String jwtToken = JwtUtils.getJwtToken(ucenterMember.getId(), ucenterMember.getNickname());
        return jwtToken;
    }

    @Override
    public void register(RegisterVo registerVo) {

        //获取注册的数据
        String mobile = registerVo.getMobile(); //手机号
        String nickname = registerVo.getNickname(); //昵称
        String password = registerVo.getPassword(); //密码
        String code = registerVo.getCode(); //验证码


        //非空判断
        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(nickname) ||
                StringUtils.isEmpty(password) || StringUtils.isEmpty(code)) {
            throw new GuliException(20001, "注册失败");
        }

        //判断验证码，获取redis验证码
        /**
         *  String redisCode = redisTemplate.opsForValue().get(mobile);
         *      if(!code.equals(redisCode)) {
         *          throw new GuliException(20001,"注册失败");
         *      }
         */

        //阿里云短信服务开通不了，所以这里用了固定的验证码“1338”
        if(Integer.parseInt(code) != 1338){
            throw new GuliException(20001,"注册失败");
        }

        //判断手机号是否重复，表里面存在相同手机号不进行添加
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile",mobile);
        Integer count = baseMapper.selectCount(wrapper);
        if(count > 0){
            throw new GuliException(20001,"注册失败");
        }

        //数据添加数据库中
        UcenterMember member = new UcenterMember();
        member.setMobile(mobile);
        member.setNickname(nickname);
        member.setPassword(MD5.encrypt(password));//密码需要加密的
        member.setIsDeleted(false); //用户不禁用
        member.setAvatar("https://sjh-first.oss-cn-hangzhou.aliyuncs.com/2022/08/27/2f396f9450ba44a280d112391031f52ffile.png");
        baseMapper.insert(member);
    }

    //根据openid判断，查询列表
    @Override
    public UcenterMember getOpenIdMember(String openid) {
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("openid",openid);
        UcenterMember member = baseMapper.selectOne(wrapper);
        return member;
    }

    //查询某一天注册人数
    @Override
    public Integer countRegisterDay(String day) {
        System.out.println(day);
        return baseMapper.countRegisterDay(day);
    }
}
