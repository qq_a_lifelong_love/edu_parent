package com.sjh.educenter.controller;


import com.sjh.commonutils.JwtUtils;
import com.sjh.commonutils.Result;
import com.sjh.commonutils.ordervo.UcenterMemberOrder;
import com.sjh.educenter.entity.UcenterMember;
import com.sjh.educenter.entity.vo.RegisterVo;
import com.sjh.educenter.service.UcenterMemberService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 会员表 前端控制器
 * </p>
 *
 * @author 宋军辉
 * @since 2023-03-26
 */
@RestController
@CrossOrigin//解决跨域问题
@RequestMapping("/educenter/member")
public class UcenterMemberController {

    @Autowired
    private UcenterMemberService memberService;

    //    登录
    @ApiOperation(value = "登录")
    @PostMapping("login")
    public Result loginUser(@RequestBody UcenterMember member) {

//member对象封装手机号和密码
        //使用service方法实现登录，返回token值(单点登录)，使用jwt生成
        String token = memberService.login(member);
        //前端创建拦截器，判断cookie中是否有token字符串，如果有，把token字符串放到header（请求头）
        System.out.println(token);
        return Result.ok().data("token", token);


    }

    //    注册
    @ApiOperation(value = "注册")
    @PostMapping("register")
    public Result registerUser(@RequestBody RegisterVo registerVo) {
        memberService.register(registerVo);
        return Result.ok();
    }

    //根据token获取用户信息
    @GetMapping("getMemberInfo")
    public Result getMemberInfo(HttpServletRequest request) {
        //调用jwt工具类的方法。根据request对象获取头信息，返回用户id
        //方法从请求头中获取“token”，将用户id返回，为了后面查询数据库根据id获取用户信息
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        //查询数据库根据用户id获取用户信息
        UcenterMember member = memberService.getById(memberId);
        return Result.ok().data("userInfo", member);
    }

//    根据用户id获取用户信息
    @ApiOperation(value = "根据用户id获取用户信息")
    @PostMapping("/getUserInfoOrder/{id}")
    public UcenterMemberOrder getUserInfoOrder(@PathVariable String id){

        UcenterMember member = memberService.getById(id);
//        把member对象里面的值复制给UcenterMemberOrder
        UcenterMemberOrder ucenterMemberOrder=new UcenterMemberOrder();
        BeanUtils.copyProperties(member,ucenterMemberOrder);
        return ucenterMemberOrder;
    }

//    查询某一天的注册人数
    @ApiOperation(value = "查询某一天的注册人数")
    @GetMapping("/countRegister/{day}")
    public Result countRegister(@PathVariable String day){
        Integer count = memberService.countRegisterDay(day);
        return Result.ok().data("countRegister",count);
    }


}

