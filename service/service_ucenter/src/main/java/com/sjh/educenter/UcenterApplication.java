package com.sjh.educenter;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 *    登录   注册
 * @Author: sjh
 * @Date: 2023-03-26
 * @Version: 1.0
 */
@SpringBootApplication
@ComponentScan("com.sjh")//指定扫描位置
@MapperScan("com.sjh.educenter.mapper")
@EnableDiscoveryClient//nacos注册
public class UcenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(UcenterApplication.class,args);
    }

//    http://localhost:8006/api/ucenter/wx/login
}
