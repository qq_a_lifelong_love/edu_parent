package com.sjh.vodtest;

import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadVideoRequest;
import com.aliyun.vod.upload.resp.UploadVideoResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.*;

import java.util.List;

/**
 * @Author: sjh
 * @Date: 2023-03-23
 * @Version: 1.0
 */
public class TestVod {

    public static void main(String[] args) {

//        上传视频的方法
        String accessKeyId = "LTAI4FvvVEWiTJ3GNJJqJnk7";
        String accessKeySecret = "9st82dv7EvFk9mTjYO1XXbM632fRbG";

        String title = "6 - What If I Want to Move Faster - upload by sdk";   //上传之后文件名称
        String fileName = "C:\\Users\\Administrator\\Desktop\\2023.3简历/小视频2.mp4";  //本地文件路径和名称
//        上传视频的方法
        UploadVideoRequest request = new UploadVideoRequest(accessKeyId, accessKeySecret, title, fileName);
//可指定分片上传时每个分片的大小，默认为2M字节
        request.setPartSize(2 * 1024 * 1024L);
        //        /* 可指定分片上传时的并发线程数，默认为1，(注：该配置会占用服务器CPU资源，需根据服务器情况指定）*/
        request.setTaskNum(1);

        UploadVideoImpl uploader = new UploadVideoImpl();
        UploadVideoResponse response = uploader.uploadVideo(request);

        if (response.isSuccess()) {
            System.out.print("VideoId=" + response.getVideoId() + "\n");
        } else {
            /* 如果设置回调URL无效，不影响视频上传，可以返回VideoId同时会返回错误码。其他情况上传失败时，VideoId为空，此时需要根据返回错误码分析具体错误原因 */
            System.out.print("VideoId=" + response.getVideoId() + "\n");
            System.out.print("ErrorCode=" + response.getCode() + "\n");
            System.out.print("ErrorMessage=" + response.getMessage() + "\n");
        }
    }

    //方法·2    根据视频的id获取视频的播放凭证
    public static void getPlayAuth(String[] args) throws ClientException {
//    根据视频的id获取视频的播放凭证
//    1创建初始化对象
        DefaultAcsClient client = InitObject.initVodClient("LTAI5tKNcdke7ZmKn6e2dsRQ", "RjCBcGz6xOQwmjfPUipjGMokjHInbE");

//    2创建视频凭证地址request和response
        GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
        GetVideoPlayAuthResponse response = new GetVideoPlayAuthResponse();

//    3向request对象里面设置视频id
        request.setVideoId("9b0975a0c8ce71ed81c07035d0b20102");
//    4调用初始化对象里边的方法得到视频的凭证
        response = client.getAcsResponse(request);
        System.out.print("playauth: " + response.getPlayAuth());

    }


    //方法·1    根据视频的id获取视频的播放地址
    public static void getPlayUrl(String[] args) throws ClientException {
//        1创建视频Id获取视频播放地址
//        1创建初始化对象
        DefaultAcsClient client = InitObject.initVodClient("LTAI5tKNcdke7ZmKn6e2dsRQ", "RjCBcGz6xOQwmjfPUipjGMokjHInbE");

//        2创建获取视频地址request和response
        GetPlayInfoRequest request = new GetPlayInfoRequest();
        GetPlayInfoResponse response = new GetPlayInfoResponse();

//        3向request对象里面设置视频id
        request.setVideoId("9b0975a0c8ce71ed81c07035d0b20102");

//        4调用初始化对象里面的方法，传递request，获取数据
        response = client.getAcsResponse(request);

//        从结果中获得数据
        List<GetPlayInfoResponse.PlayInfo> playInfoList = response.getPlayInfoList();
//        播放地址
        for (GetPlayInfoResponse.PlayInfo playInfo : playInfoList) {
            System.out.print("PlayInfo.PlayURL = " + playInfo.getPlayURL() + "\n");
        }
//        Base信息
        System.out.print("VideoBase.Title = " + response.getVideoBase().getTitle() + "\n");


    }
}
