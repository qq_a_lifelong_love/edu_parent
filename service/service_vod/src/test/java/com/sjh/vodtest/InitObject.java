package com.sjh.vodtest;

import com.aliyun.oss.ClientException;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.profile.DefaultProfile;

/**
 * @Author: sjh
 * @Date: 2023-03-22
 * @Version: 1.0
 */
public class InitObject {
//    初始化对象，创建DefaultAcsClient对象
    public static DefaultAcsClient initVodClient(String accessKeyId,String accessKeySecret) throws ClientException{

        String regionId = "cn-shanghai";//点播服务接入区
        DefaultProfile profile=DefaultProfile.getProfile(regionId,accessKeyId,accessKeySecret);
        DefaultAcsClient client=new DefaultAcsClient(profile);
        return client;

    }

}
