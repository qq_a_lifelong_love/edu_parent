package com.sjh.vod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.CrossOrigin;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Author: sjh
 * @Date: 2023-03-23
 * @Version: 1.0
 */
//不去扫描数据库
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@ComponentScan(basePackages = {"com.sjh"})
@EnableDiscoveryClient//nacos注册
public class VodApplication {

//    http://localhost:8003/swagger-ui.html#/

    public static void main(String[] args) {
        SpringApplication.run(VodApplication.class,args);
    }
}
