package com.sjh.vod.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Author: sjh
 * @Date: 2023-03-23
 * @Version: 1.0
 */
public interface VodService {

    //    上传视频到阿里云
    String uploadVideoAly(MultipartFile file);

    //    删除多个阿里云视频
    void removeMoreAlyVideo(List videoIdList);
}
