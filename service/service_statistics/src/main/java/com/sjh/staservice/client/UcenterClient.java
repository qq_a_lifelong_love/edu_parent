package com.sjh.staservice.client;

import com.sjh.commonutils.Result;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Author: sjh
 * @Date: 2023-04-05
 * @Version: 1.0
 */
@Component//交给spring管理
@FeignClient("service-ucenter")//指定调用的名字
public interface UcenterClient {

    @ApiOperation(value = "查询某一天的注册人数")
    @GetMapping("/educenter/member/countRegister/{day}")
    public Result countRegister(@PathVariable("day") String day);
}
