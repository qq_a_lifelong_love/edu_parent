package com.sjh.staservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sjh.commonutils.Result;
import com.sjh.staservice.client.UcenterClient;
import com.sjh.staservice.entity.StatisticsDaily;
import com.sjh.staservice.mapper.StatisticsDailyMapper;
import com.sjh.staservice.service.StatisticsDailyService;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务实现类
 * </p>
 *
 * @author 宋军辉
 * @since 2023-04-04
 */
@Service
public class StatisticsDailyServiceImpl extends ServiceImpl<StatisticsDailyMapper, StatisticsDaily> implements StatisticsDailyService {

    @Autowired
    private UcenterClient ucenterClient;


    //    统计某一天注册人数,生成统计数据
    @Override
    public void registerCount(String day) {

        //    添加记录之前先删除相同日期的数据
        QueryWrapper<StatisticsDaily> wrapper = new QueryWrapper<>();
        wrapper.eq("date_calculated", day);
        baseMapper.delete(wrapper);


        Result registerResult = ucenterClient.countRegister(day);
//        得到注册人数
        Integer countRegister = (Integer) registerResult.getData().get("countRegister");

//        把获取到的数据添加到数据库，统计分析表里面
        StatisticsDaily sta = new StatisticsDaily();
        sta.setRegisterNum(countRegister);//注册人数
        sta.setDateCalculated(day);//统计日期
        sta.setVideoViewNum(RandomUtils.nextInt(100, 200));//每日播放视频数
        sta.setLoginNum(RandomUtils.nextInt(100, 200));//登陆人数
        sta.setCourseNum(RandomUtils.nextInt(100, 200));//每日新增课程数

        baseMapper.insert(sta);

    }

    //    图表显示，返回两部分数据，日期json格式，以及数量json格式
    @Override
    public Map<String, Object> getShowDate(String type, String begin, String end) {

//        根据条件查询对应的数据
        QueryWrapper<StatisticsDaily> wrapper = new QueryWrapper<>();
        wrapper.between("date_calculated", begin, end);
        wrapper.select("date_calculated", type);
        List<StatisticsDaily> staList = baseMapper.selectList(wrapper);

//        因为返回有两部分数据：日期 和日期对应的数量
//        前端要求数组json结构，对应后端java代码是list集合
//        创建两个list集合，一个日期list 一个数量list
        List<String> date_calculatedList = new ArrayList<>();
        List<Integer> numDataList = new ArrayList<>();

//        遍历查询所有数据list集合，进行封装
        for (int i = 0; i < staList.size(); i++) {
            StatisticsDaily daily = staList.get(i);
//            封装日期list集合
            date_calculatedList.add(daily.getDateCalculated());

//            封装对应数量
            switch (type) {
                case "register_num":
                    numDataList.add(daily.getRegisterNum());
                    break;
                case "login_num":
                    numDataList.add(daily.getLoginNum());
                    break;
                case "video_view_num":
                    numDataList.add(daily.getVideoViewNum());
                    break;
                case "course_num":
                    numDataList.add(daily.getCourseNum());
                    break;
                default:
                    break;
            }

        }
//        把封装后的两个list集合放到map集合中，进行返回
        Map<String, Object> map = new HashMap<>();
        map.put("date_calculatedList", date_calculatedList);
        map.put("numDataList", numDataList);
        return map;
    }


}
