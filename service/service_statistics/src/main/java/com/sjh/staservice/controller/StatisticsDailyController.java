package com.sjh.staservice.controller;


import com.sjh.commonutils.Result;
import com.sjh.staservice.service.StatisticsDailyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 前端控制器
 * </p>
 *
 * @author 宋军辉
 * @since 2023-04-04
 */
@RestController
@RequestMapping("/staservice/sta")
@CrossOrigin//解决跨域问题
public class StatisticsDailyController {

    @Autowired
    private StatisticsDailyService staService;

    //    统计某一天注册人数,生成统计数据
    @ApiOperation(value = "统计某一天注册人数")
    @PostMapping("/registerCount/{day}")
    public Result registerCount(@PathVariable String day) {
        staService.registerCount(day);
        return Result.ok();
    }

    //    图表显示，返回两部分数据，日期json格式，以及数量json格式
    @ApiOperation(value = "图表显示，返回两部分数据，日期json格式，以及数量json格式")
    @GetMapping("/showDate/{type}/{begin}/{end}")
    public Result showDate(@PathVariable String type,
                           @PathVariable String begin,
                           @PathVariable String end) {

        Map<String, Object> map = staService.getShowDate(type, begin, end);

        return Result.ok().data(map);
    }

}

