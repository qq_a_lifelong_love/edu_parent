package com.sjh.staservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @Author: sjh
 * @Date: 2023-04-05
 * @Version: 1.0
 */
@SpringBootApplication
@EnableDiscoveryClient//nacos注册
@EnableFeignClients//服务调用
@MapperScan("com.sjh.staservice.mapper")
@ComponentScan(basePackages = {"com.sjh"})
@EnableScheduling//开启定时任务的注解
public class StaApplication {
    public static void main(String[] args) {
        SpringApplication.run(StaApplication.class,args);
    }
}
