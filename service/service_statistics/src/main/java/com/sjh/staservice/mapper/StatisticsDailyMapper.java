package com.sjh.staservice.mapper;

import com.sjh.staservice.entity.StatisticsDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网站统计日数据 Mapper 接口
 * </p>
 *
 * @author 宋军辉
 * @since 2023-04-04
 */
public interface StatisticsDailyMapper extends BaseMapper<StatisticsDaily> {

}
