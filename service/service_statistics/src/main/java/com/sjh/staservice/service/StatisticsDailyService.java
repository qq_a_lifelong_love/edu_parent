package com.sjh.staservice.service;

import com.sjh.staservice.entity.StatisticsDaily;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务类
 * </p>
 *
 * @author 宋军辉
 * @since 2023-04-04
 */
public interface StatisticsDailyService extends IService<StatisticsDaily> {

    //    统计某一天注册人数,生成统计数据
    void registerCount(String day);

    //    图表显示，返回两部分数据，日期json格式，以及数量json格式
    Map<String, Object> getShowDate(String type, String begin, String end);
}
