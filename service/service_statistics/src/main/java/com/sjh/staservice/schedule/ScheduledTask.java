package com.sjh.staservice.schedule;

import com.sjh.staservice.service.StatisticsDailyService;
import com.sjh.staservice.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author: sjh
 * @Date: 2023-04-06
 * @Version: 1.0
 */

@Component//交给spring容器进行管理
public class ScheduledTask {

    @Autowired
    private StatisticsDailyService statisticsDailyService;


//  "0/5 * * * * ?"  每隔5秒执行一次这个方法
   /* @Scheduled(cron = "0/5 * * * * ?")
    public void task1(){
        System.out.println("******此任务执行了"+new Date());
    }*/


    //在每天凌晨1点，把前一天的数据数据查询进行添加
    @Scheduled(cron = "0 0 1 * * ?")
    public void task2(){
       statisticsDailyService.registerCount(
               DateUtil.formatDate(DateUtil.addDays(new Date(),-1))
       );
    }

}
