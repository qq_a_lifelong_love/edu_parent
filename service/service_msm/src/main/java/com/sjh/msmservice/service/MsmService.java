package com.sjh.msmservice.service;

import java.util.Map;

/**
 * @Author: sjh
 * @Date: 2023-03-26
 * @Version: 1.0
 */
public interface MsmService {

    //    发送短信的方法
    Boolean send(Map<String, Object> param, String phone);
}
