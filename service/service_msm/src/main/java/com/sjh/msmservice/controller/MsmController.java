package com.sjh.msmservice.controller;

import com.sjh.commonutils.Result;
import com.sjh.msmservice.service.MsmService;
import com.sjh.msmservice.utils.RandomUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author: sjh
 * @Date: 2023-03-26
 * @Version: 1.0
 */
//发送短信
@RestController
@RequestMapping("/edumsm/msm")
@CrossOrigin//解决跨域
@EnableDiscoveryClient
public class MsmController {


    @Autowired
    private MsmService msmService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    //    发送短信的方法
    @ApiOperation(value = "发送短信的方法")
    @GetMapping("/send/{phone}")
    public Result sendMsm(@PathVariable String phone) {

        //1 从redis获取验证码，如果获取到直接返回
        String code = redisTemplate.opsForValue().get(phone);
        if(!StringUtils.isEmpty(code)) {
            return Result.ok();
        }
        //2 如果redis获取 不到，进行阿里云发送
//        生成随机值，传递阿里云进行发送
        code = RandomUtil.getFourBitRandom();//生成4位数的验证码
        Map<String, Object> param = new HashMap<>();
        param.put("code", code);
//        调用service发送短信的方法
        Boolean isSend = msmService.send(param, phone);
        if (isSend) {
            //发送成功，把发送成功验证码放到redis里面
            //设置有效时间为5分钟
            redisTemplate.opsForValue().set(phone, code, 5, TimeUnit.MINUTES);
            return Result.ok();
        } else {
            return Result.error().message("短信发送失败");
        }
    }
}
