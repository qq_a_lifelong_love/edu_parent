package com.sjh.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author: sjh
 * @Date: 2023-04-07
 * @Version: 1.0
 */

@SpringBootApplication
@EnableDiscoveryClient//nacos注解
public class ApiGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiGatewayApplication.class, args);
    }
}
