package com.sjh.commonutils;

/**
 * @Author: sjh
 * @Date: 2022-08-09
 * @Version: 1.0
 */
public interface ResultCode {
    public static Integer SUCCESS = 20000;
    public static Integer ERROR = 20001;
}
